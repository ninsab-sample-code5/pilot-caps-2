// Dependencies
const express = require('express');
const mongoose = require('mongoose');
// Add dependencies for routes
const userRoute = require('./routes/userRoutes.js')
const productRoute = require('./routes/productRoutes.js')

// Database Connect
mongoose.connect("mongodb+srv://ninya:ninya0258@naenaens.9qtap.mongodb.net/ScribblesEcommerce?retryWrites=true&w=majority");
let db = mongoose.connection;
db.on("error", console.error.bind(console, 'Connection Error'));
db.once("open", () => console.log('Successfully connected to Database!'))

// Server Setup
const server = express();
const address = 3000;
server.use(express.json())

// Routes
server.use('/products', productRoute);
server.use('/users', userRoute);

// Server Response
server.listen(address, () => {
    console.log(`Server is running on port ${address}`)
});
// [SECTION] Dependencies and Modules
const Product = require('../models/Product.js'); //data model

// [SECTION] Functionalities


	module.exports.createProduct = (requestBody) => {
		
		let pName = requestBody.name
		let pDes = requestBody.description
		let pPrice = requestBody.price

		let newProduct = new Product ({
			productName: pName,
			productDescription: pDes,
			productPrice: pPrice,
		})

		return newProduct.save().then((savedProduct, error) => {
			if (error) {
				return 'There was an error in adding the product.';
			} else {
				return 'A product has been created successfully.';
			}
		})
	}
// retrieving all courses
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}

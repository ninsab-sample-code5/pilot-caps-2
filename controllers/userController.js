
const User = require('../models/User.js'); 


	module.exports.createUser = (requestBody) => {

		let fName = requestBody.firstName
		let lName = requestBody.lastName
		let age = requestBody.age

		let newUser = new User ({
			firstName: fName,
			lastName: lName,
			age: age
		})

		return newUser.save().then((savedUser, error) => {
			if (error) {
				return 'There was an error in saving the file.';
			} else {
				return 'A user has been created successfully.';
			}
		})
	}

module.exports.getAllUsers = () => {

	return User.find({}).then(result => {

		return result;
	})
}



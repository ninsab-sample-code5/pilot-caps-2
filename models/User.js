const mongoose = require('mongoose');

const userBlueprint = new mongoose.Schema ({
    firstName: {
        type: String,
        required: [true, "First Name is Required."]
    },
    lastName: {
        type: String,
        required: [true, "Last Name is Required."]
    },
    age: {
        type: String,
        required: [true, "Age is Required."]
    },
    isAdmin: { //i want this to have a default value
        type: Boolean,
        default: false
    }
});

module.exports = mongoose.model("User", userBlueprint);

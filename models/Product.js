const mongoose = require('mongoose');

const productBlueprint = new mongoose.Schema ({
		productName: {
			type: String,
			required: [true, "Product Name is Required."]
		},
		productDescription: {
			type: String,
			required: [true, "Description for this product is Required."]
		},
		productPrice: {
			type: Number,
			required: [true, "Price is Required."]
		},
		isOffered: { 
			type: Boolean,
			default: true
		}
});


module.exports = mongoose.model("Product", productBlueprint);





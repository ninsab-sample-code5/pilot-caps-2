// [SECTION] Dependencies and Modules
const express = require('express') 
const controller = require('../controllers/userController.js')

// [Routing Components]
const route = express.Router();

// [SECTION] Static Routes
	// Create User
	route.post('/create', (req, res) => {
		let userInfo = req.body;
		controller.createUser(userInfo).then(result => res.send(result))
	});

// Retrieve All Users
	route.get('/', (req, res) => {
		controller.getAllUsers().then(result => res.send(result))
	});



module.exports = route; 
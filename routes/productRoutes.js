const express = require('express') 
const controller = require('../controllers/productController.js')

const route = express.Router();

route.post('/create', (req, res) => {
	let productInfo = req.body;
	controller.createProduct(productInfo).then(result => res.send(result))	
});

route.get('/', (req, res) => {
	controller.getAllProducts().then(result => res.send(result))
});

module.exports = route; 